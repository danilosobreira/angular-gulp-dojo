

(function(angular){
  'use strict';
  var app = angular.module('angular-gulp-dojo');
  app.controller('TodoCtrl', function($scope, $http){
    // $scope.$watch('name', function(newValue, oldValue){
    //  console.log(newValue);
    //});

    var url = 'https://angular-gulp-dojo-backend.herokuapp.com/api/task/';
    $http.get(url)
    .success(function(tasks){
      console.log(JSON.stringify(tasks));
      $scope.tasks = tasks;
    });

    $scope.save = function(task){
      $http.post(url,task)
      .success(function(){
        console.log("Cadastrado com Sucesso");
      });
    }

    $scope.delete = function(id)
    {
      var url_excluir = 'https://angular-gulp-dojo-backend.herokuapp.com/api/task/' + id;
      $scope.loading = true;
      $http.delete(url_excluir)
      .success(function(){
        $scope.loading = false;
        console.log("Exclusão realizada");
      })
      .error(function(mensagem){
        console.log(mensagem);
      });
    }

    /*
    $scope.editar = function(id)
    {
      var url_editar = 'https://angular-gulp-dojo-backend.herokuapp.com/api/task/' + id;

      $http.put(url_editar)
      .success(function(){
        console.log("Editado com sucesso");
      })
      .error(function(mensagem){
        console.log(mensagem);
      });
    }*/
  });
})(angular);
